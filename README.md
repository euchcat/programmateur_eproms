# PROGRAMMATEUR D'EPROMs (UNIVERSEL)

### Avertissement:
Ce projet date initialement de 1998 et est sur ce site en tant qu'archive  
"antique"  

- Date mise en ligne GitLab: 01.04.2020
- reconstruction du projet fossil-CVS  
(Date dernières modification: 10.03.2012)

## PROGRAMMATEUR D'EPROM
Un programmateur D'EPROM de type M2716 et M2732  
Fonctionne sous MS-DOS, Le programmateur est relié au PC (de l'époque)  
par le port parallèle. :)  

#### Le but: 
- Programmer et lire différents types d'eprom, tous les moyens sont envisageables.
- Archiver (Museum du développement)
- Nostalgie (Pascal, Basic, Binaires)

***Note:*** Schéma et documentation de l'époque à ajouter. (à retrouver aussi...)  

#### Licence:
 Sous licence MIT (voir fichier LICENSE)  
 
---